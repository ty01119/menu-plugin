<?php
/*
	Plugin Name: Menu manager plugin 
	Plugin URI: http://www.test.com
	Description: Menu manager plugin 
	Author: Willaim Tian
	Version: 1.1.0
	Author URI: http://www.multisupport.co.nz
*/


defined('WP_PLUGIN_URL') or die('Restricted access');

global $wpdb;
$pluginName = "RestoMan";
define('WPPRESTO_PATH',ABSPATH.PLUGINDIR.'/'. $pluginName .'/');
define('WPPRESTO_PLUGIN_URL',WP_PLUGIN_URL.'/'. $pluginName .'/');
define('WPPRESTO_MENU_DB', strtolower($wpdb->prefix.'WPPRESTO_menus'));
define('WPPRESTO_CATEGORY_DB', strtolower($wpdb->prefix.'WPPRESTO_categories'));
define('WPPRESTO_ITEM_DB', strtolower($wpdb->prefix.'WPPRESTO_items'));

define('WPPRESTO_ADMIN_URL',"main_menu");
define('WPPRESTO_ADMIN_PARSE',"page");
define('WPPRESTO_CAPABILITY','edit_pages');
define('WPPRESTO_EXTENDED_DB_VERSION','1.0');
define('WPPRESTO_HELP','help');

require_once("admin/functions.php");
require_once("lib/db_setup.php");
require_once("lib/class.menu.php");
require_once("lib/class.category.php");
require_once("lib/class.item.php");
require_once("views/view.menu.php");
require_once("views/print.menu.php");


/* run setup scripts on activation */
register_activation_hook(__FILE__,'WPPRESTO_install_plugin');

function wpresto_admin_menu_setup() {
  $title = "RestoMan Restaurant Menu Manager - ";
  add_menu_page($title . "Build Menus", "Manage Menus", WPPRESTO_CAPABILITY, "main_menu", "wpresto_admin_menulist","",58);
  //add_submenu_page( "main_menu", $title.'Menu Items', "Menu Items", WPPRESTO_CAPABILITY, "menuitems", "wpresto_admin_menuitem" );
  add_submenu_page( "main_menu", $title.'Food Items', "Food Items", WPPRESTO_CAPABILITY, "fooditems", "wpresto_admin_fooditems" );
	add_submenu_page( "main_menu", $title.'Categories', "Categories", WPPRESTO_CAPABILITY, "category", "wpresto_admin_category" );
  add_submenu_page( "main_menu", $title.'Setings', "Setings", WPPRESTO_CAPABILITY, "settings", "wpresto_admin_settings" );
}
add_action('admin_menu', 'wpresto_admin_menu_setup');


/* Load CSS and JavaScript */
function wpresto_admin_css(){
  wp_enqueue_style('wpresto-admin-style', WPPRESTO_PLUGIN_URL.'css/admin-style.css');
  wp_enqueue_script("bootstrap-js", WPPRESTO_PLUGIN_URL.'js/jquery.tablesorter.min.js');

  wp_enqueue_script("jquery-ui-js", WPPRESTO_PLUGIN_URL.'js/jquery-ui.min.js');
  wp_enqueue_script("jquery-table-sort", WPPRESTO_PLUGIN_URL.'js/jquery.tablednd.js');
}
add_action('admin_init', 'wpresto_admin_css',20);

function wpresto_css() {
  if(!is_admin()){
//    wp_enqueue_style('wpresto-style', WPPRESTO_PLUGIN_URL.'css/view-style.css');
//    wp_enqueue_style('bootstrap-style', WPPRESTO_PLUGIN_URL.'css/b.min.css');
//    wp_enqueue_style('bootstrap-style', WPPRESTO_PLUGIN_URL.'css/magnific-popup.css');
  } 
}
add_action('wp_print_styles', 'wpresto_css',10);


function bootstrap_style() {
  $custom_css = ".mycolor{ background: #FFF000 ";
  wp_add_inline_style( 'wpresto1-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'bootstrap_style' );

function wpresto_js() {
  if(!is_admin()){
    //wp_enqueue_script("jquery-js", WPPRESTO_PLUGIN_URL.'js/jquery.min.js');
  if(wp_script_is('jquery')) {
    // do nothing
    wp_enqueue_script("jquery-ui-js", WPPRESTO_PLUGIN_URL.'js/jquery-ui.min.js');
    wp_enqueue_script("bootstrap-js", WPPRESTO_PLUGIN_URL.'js/b.min.js');
    wp_enqueue_script("bootstrap-js", WPPRESTO_PLUGIN_URL.'js/jquery.magnific-popup.min.js');
  } else {
    //wp_enqueue_script("jquery-min-js", WPPRESTO_PLUGIN_URL.'js/jquery.min.js');
//	  wp_enqueue_script('jquery');
  }

  }
}
add_action('wp_enqueue_scripts', 'wpresto_js',20);

/*** End Load CSS and JavaScript ***/

/*** Use Shortcode API to output menus on frontend */
function wpresto_shortcode_handler($atts, $content=null, $code=""){

  extract( shortcode_atts( array('categories' => 'all'), $atts ) );
  $categories = str_replace( ' ', '', $categories );
  if( !empty($categories) && $categories != 'all' && preg_match( '/^[-,0-9]+$/', $categories ) ){
    $categories = explode( ',', $categories );
  }
  return wpresto_display_menu($atts, $categories);
  
}
add_shortcode('WP_RestoMenu', 'wpresto_shortcode_handler');


function wpresto_export($wp) {
    // only process requests POST'ed to "/?wprmm-routing=export"
    if (array_key_exists('wpresto-action', $wp->query_vars) && $wp->query_vars['wpresto-action'] == 'print') {
      echo wpresto_print_menu($_GET['menu_id']);
      die();exit();
    }
}
add_action('parse_request', 'wpresto_export');

function wpresto_parse_query_vars($vars) {
    $vars[] = 'wpresto-action';
    return $vars;
}
add_filter('query_vars', 'wpresto_parse_query_vars');


?>
