<div class="wrap WPRESTO">
  <h2 class="left">Restaurant Menu Manager - Setup Categories</h2>
  <div class="clear"></div>
  <hr />
  
<?php 


if(isset($_POST['wpresto_delete_cat']['category']))
{
	$del_cat_id = $_POST['wpresto_delete_cat']['category_id'];
	$cat = new WPRESTO_Category();
	$cat->destroy($del_cat_id);
	echo wpresto_alert_msg("Item <b>" . $_POST['wpresto_delete_cat']['name'] . "</b> successfully deleted! ");

}

$menu = new WPRESTO_Menu((int) $_GET['menu_id']);
$category = new WPRESTO_Category();
$categories = $category->get_all();
//$categories = $category->get_by_id();
//echo "cat: " . $categories;
//print_r($categories);
?>


  <table class="widefat">
  <thead>
    <tr>
      <th>Name</th>
      <th>名称</th>
      <th>Edit</th>
      <th>Parent Menu</th>

      <th>Order</th>
      <th>Active</th>
      <th>ID</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th></th>
      <th></th>
      <th></th>

      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </tfoot>
  <tbody>
    <?php foreach($categories as $c): ?>
     <tr>
       <td><strong><?php echo $c->name; ?></strong></td>
         <td><strong><?php echo $c->name_cn; ?></strong></td>
       <td><a href="admin.php?page=main_menu&action=edit_category&cat_id=<?php echo $c->id; ?>">Edit Category</a></td>
       <td><?php
			$pmenu = new WPRESTO_Menu($c->menu_id);
			echo $pmenu->name;
	   ?></td>
   
       <td><?php echo $c->display_order; ?></td>
       <td><?php echo ($c->active == 1) ? 'Yes' : 'No'; ?></td>
       <td><?php echo $c->id; ?></td>
       <td>
         <form method="post" action="#">
           <input type="hidden" name="wpresto_delete_cat[menu_id]" value="<?php echo $c->menu_id;?>" />
           <input type="hidden" name="wpresto_delete_cat[category_id]" value="<?php echo $c->id;?>" />
           <input type="hidden" name="wpresto_delete_cat[name]" value="<?php echo $c->name;?>" />
           <input type="submit" class="button" name="wpresto_delete_cat[category]" value="Delete" onclick="return confirm('Are you sure you want to delete this category?')" />
         </form>
       </td>
     </tr>
    <?php endforeach; ?>
  </tbody>
  </table>

  <div class="WPRESTO-admin-nav">
    <p>
      <a class="button-primary" href="admin.php?page=main_menu&action=create_category">+ Add New Category</a>&nbsp;
      <a class="button" href="">&laquo;back to Menus</a>&nbsp;
    </p>
  </div>



</div>
