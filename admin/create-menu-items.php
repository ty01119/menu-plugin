
<div class="wrap wpresto">
  <h2 class="left">Restaurant Menu Manager - Add Item</h2>
  <div class="clear"></div>
  <hr />

<?php

$menu_id = $_GET['menu_id'];    

if(isset($_POST['save_item'])) {
    $newItems = $_POST['wpresto'];
    $item = new WPRESTO_Item();
    if ($_POST['wpresto']['image']=='') $newItems['image']= WPPRESTO_PLUGIN_URL."/images/defaultFood.jpg";
    $new = $item->create($newItems);
    echo wpresto_alert_msg("Item <b>" . $newItems['name'] . "</b> successfully added! ");

    $uploaddir = WPPRESTO_PATH . "images/";
    $fileName = "food-".$new.".jpg";
    $uploadfile = $uploaddir . $fileName;
    $uploadOk = 0;
    $updatedItems['id'] = $new;
    if (move_uploaded_file($_FILES['item_image']['tmp_name'], $uploadfile)) $uploadOk = 1;
    if($uploadOk==1) $updatedItems['image'] = WPPRESTO_PLUGIN_URL."/images/".$fileName;
    $item->update($updatedItems);
}

$category = new WPRESTO_Category();
$categories = $category->get_all_by_menu($menu_id);
$menuLoad = new WPRESTO_Menu($menu_id);

?>

<script type="text/javascript">
$(document).ready(function(){
       $("#upload_image_file").change(function() {
           //alert("asd");
           readURL(this);
       });
   });
    function readURL(input) {
           if (input.files && input.files[0]) {
               var reader = new FileReader();
               reader.onload = function(e) {
                   $('#previewHolder').attr('src', e.target.result);
                   $('#previewHolder').attr('width', 200);
                   
               };
               reader.readAsDataURL(input.files[0]);
           }
       }
</script>



  <p class="wpresto-breadcrumb">
    <a href="admin.php?page=main_menu">Menus</a> &raquo; 
    <a href="admin.php?page=main_menu&action=edit_item&menu_id=<?php echo $menuLoad->id;?>"><?php echo $menuLoad->name; ?></a> &raquo; 
    <a href="">Add Item</a>
  </p>

  <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
    <table class="form-table">
      <tbody>
        <tr valign="top">
          <th scope="row"><label for="wpresto[name_cn]">名称</label></th>
          <td><input name="wpresto[name_cn]" type="text" value="" class="regular-text">
              <span class="description">中文名称.</span>
          </td>
        </tr>
        <tr valign="top">
          <th scope="row"><label for="wpresto[name]">Name</label></th>
          <td><input name="wpresto[name]" type="text" value="" class="regular-text">
              <span class="description">Display name for this item.</span>
          </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[name_cn]">名称</label></th>
            <td><input name="wpresto[name_cn]" type="text" value="" class="regular-text">
                <span class="description">Display name for this item.</span>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[description_cn]">描述</label></th>
            <td><textarea name="wpresto[description_cn]" class="large-text code"></textarea>
                <span class="description">中文描述.</span>
            </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[description]">Description</label></th>
          <td><textarea name="wpresto[description]" class="large-text code"></textarea>
              <span class="description">Display description for this item.</span>
          </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[description_cn]">描述</label></th>
            <td><textarea name="wpresto[description_cn]" class="large-text code"></textarea>
                <span class="description">Display description for this item.</span>
            </td>
        </tr>

        <tr valign="top">
          <th scope="row">Upload Image</th>
          <td>
            <label for="upload_image">
              <input id="upload_image" type="text" size="36" name="wpresto[image]" value="" />
              <span class="description">Enter an URL or upload an image for this item.</span><br>
              <input id="upload_image_file" type="file" value="Upload Image" name="item_image"/>
              <span class="description">Upload an image for this item.</span><br>
              <img id="previewHolder" src="#" alt="your image" width=0/>
            </label>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[price]">Price</label></th>
          <td><input name="wpresto[price]" type="text" value="" class="regular-text">
              <span class="description">Displayed Price. Include denomination if desired.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[second_price]">Secondary Price</label></th>
          <td><input name="wpresto[second_price]" type="text" value="" class="regular-text">
              <span class="description">Use to display half and full order prices, or price with special side etc.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[show_price]">Show Price</label></th>
          <td>
            <input type="checkbox" name="wpresto[show_price]" value="1"/>
            <span class="description">Display the price for this item.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[category_id]">Category</label></th>
          <td>
            <select name="wpresto[category_id]" class="regular-text code">
              <?php foreach($categories as $cat):?>
                <option value="<?php echo $cat->id;?>" <?php echo ($item->category_id == $cat->id)? 'selected' : '';?>><?php echo $cat->name;?></option>
              <?php endforeach;?>
              <option value="0" <?php echo ($item->category_id == "0")? 'selected' : '';?>>None (hidden)</option>
            </select>
            <span class="description">Category for this item in this menu. Controls layout of items. Setup Categories and then items.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[active]">Active</label></th>
          <td>
            <input type="checkbox" name="wpresto[active]" value="1"/>
            <span class="description">Display this item.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[display_order]">Display Order</label></th>
          <td><input name="wpresto[display_order]" type="text" value="" class="regular-text">
              <span class="description">Order in which this item is displayed to users.</span>
          </td>
        </tr>

      </tbody>
    </table>

    <br />

    <input type="hidden" name="wpresto[menu_id]" value="<?php echo $menu_id?>" />
    <div class="wpresto-admin-nav">
      <p>
        <input class="button-primary" class="left" type="submit" name="save_item" value="Save Item" />
        <a class="button" href="admin.php?page=main_menu&action=edit_item&menu_id=<?php echo $menu_id; ?>">&laquo;back to Items</a>&nbsp;
      </p>
    </div>


  </form>

</div>
