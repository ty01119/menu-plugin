<div class="wrap WPRESTO">
  <h2 class="left">Restaurant Menu Manager - Edit Menu Category</h2>
  <div class="clear"></div>
  <hr />
<?php
if(isset($_POST['save_category'])) {
	$updatedCat = $_POST['wpresto'];
	$updatedCat['menu_id'] = $_POST['menu_id'];
	if (!isset($_POST['wpresto']['show_title'])) $updatedCat['show_title']=0;  
	if (!isset($_POST['wpresto']['show_description'])) $updatedCat['show_description']=0;  
	if (!isset($_POST['wpresto']['active'])) $updatedCat['active']=0;  
	$category = new WPRESTO_Category();
	$cat = $category->update($updatedCat);
	//print_r($_POST);
	//print_r($updatedCat);
	echo wpresto_alert_msg("Category <b>" . $updatedCat['name'] . "</b> successfully updated! ");
}

$cat_id = $_GET['cat_id'];
$category = new WPRESTO_Category((int) $_GET['cat_id']);

$menuList = new WPRESTO_MENU();
$menus = $menuList->get_all();


?>


  <p class="wpresto-breadcrumb">
    <a href="admin.php?page=main_menu">Menus</a> &raquo; 
	<a href="admin.php?page=category">Categories</a> &raquo; 
	<a href="">Edit <?php echo $category->name;?></a>
	
  </p>
  <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <table class="form-table">
      <tbody>

        <tr valign="top">
          <th scope="row"><label for="wpresto[name_cn]">名称</label></th>
          <td><input name="wpresto[name_cn]" type="text" value="<?php echo $category->name_cn;?>" class="regular-text">
              <span class="description">分类名称</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[name]">Name</label></th>
          <td><input name="wpresto[name]" type="text" value="<?php echo $category->name;?>" class="regular-text">
              <span class="description">Display name for this category.</span>
          </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[description_cn]">描述</label></th>
            <td><textarea name="wpresto[description_cn]" class="large-text code"><?php echo $category->description_cn;?></textarea>
                <span class="description">分类描述</span>
            </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[description]">Description</label></th>
          <td><textarea name="wpresto[description]" class="large-text code"><?php echo $category->description;?></textarea>
              <span class="description">Display description for this category.</span>
          </td>
        </tr>

		<tr valign="top">
          <th scope="row"><label for="wpresto[menu_id]">Link to Menu</label></th>
          <td>
            <select name="menu_id" class="regular-text code">
              <?php foreach($menus as $m):?>
                <option value="<?php echo $m->id;?>" <?php echo ($category->menu_id==$m->id) ? "selected" : "" ;?>><?php echo $m->name;?></option>
              <?php endforeach;?>
              <option value="0" <?php echo ($category->menu_id==0) ? "selected" : "" ;?>>None (hidden)</option>
            </select>
            <span class="description">Link this category to a menu</span>
          </td>
        </tr>
		
        <tr valign="top">
          <th scope="row"><label for="wpresto[show_title]">Show Title?</label></th>
          <td><input type="checkbox" name="wpresto[show_title]" value="1" <?php echo ($category->show_title == 1) ? 'checked' : '';?>/>
              <span class="description">Display title text on frontend for category.</span>
          </td>
        </tr>
        <tr valign="top">
          <th scope="row"><label for="wpresto[show_description]">Show Description?</label></th>
          <td><input type="checkbox" name="wpresto[show_description]" value="1" <?php echo ($category->show_description == 1) ? 'checked' : '';?>/>
              <span class="description">Display description text on frontend for category.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[active]">Active</label></th>
          <td>
            <input type="checkbox" name="wpresto[active]" value="1" <?php echo ($category->active == 1) ? 'checked' : '';?>/>
            <span class="description">Display items in this category.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[display_order]">Display Order</label></th>
          <td><input name="wpresto[display_order]" type="text" value="<?php echo $category->display_order;?>" class="regular-text">
              <span class="description">Order in which this category is displayed to users.</span>
          </td>
        </tr>

      </tbody>
    </table>

    <br />

    <input type="hidden" name="wpresto[id]" value="<?php echo $category->id;?>" />

    <div class="WPRESTO-admin-nav">
      <p>
        <input class="button-primary" class="left" type="submit" name="save_category" value="Save Category" />&nbsp;
        <!--a class="button" href="admin.php?page=main_menu">&laquo;back</a-->&nbsp;
      </p>
    </div>


  </form>

</div>
