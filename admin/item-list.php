<div class="wrap WPRESTO">
  <h2 class="left">Restaurant Menu Manager - Item List</h2>
  <div class="clear"></div>
  <hr />

<?php 

if(isset($_POST['wpresto_delete']['item']))
{
	$del_menu_id = $_POST['wpresto_delete']['menu_id'];
	$del_item_id = $_POST['wpresto_delete']['item_id'];
	$item3 = new WPRESTO_Item();
	$item3->destroySingle($del_item_id, $del_menu_id);
	echo wpresto_alert_msg("Item <b>" . $_POST['wpresto_delete']['item_name'] . "</b> successfully deleted! ");
}

$menu_id = $_GET['menu_id'];
$menu = new WPRESTO_Menu($menu_id);
$item = new WPRESTO_Item();
$items = $item->get_all();

$category = new WPRESTO_Category();
$categories = $category->get_all($menu->id);

?>

<style>
.myDragClass {
	background-color:orange;
}

.hover {
	/*/background-color:orange;*/
}

</style>
<script type="text/javascript">
	jQuery(document).ready(function($)
		{ 
		    $("#tem-list").tablesorter(); 
		} 
	); 
</script>


  <p class="wpresto_breadcrumb">
    <a href="admin.php?page=main_menu">Menus</a> &raquo; 
    <a href="">Item List</a>
  </p>

  <table class="widefat" id="item-list">
  <thead>
    <tr>
      <th>Name</th>
      <th>名称</th>
      <th>Parent Menu</th>
      <th>Category</th>
      <th>Price</th>
      <th>Image</th>
      <th>Order</th>
      <th>Active</th>
      <th>Edit</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($items as $i): ?>
           <tr id="<?php echo $i->id;?>" style="border-bottom: 1px solid">
	       <td><strong><?php echo $i->name; ?></strong></td>
           <td><strong><?php echo $i->name_cn; ?></strong></td>
	       <?php
	       		$pmenu = new WPRESTO_Menu($i->menu_id);
	       ?>
	       <td><a href="admin.php?page=main_menu&action=edit_single_menu&menu_id=<?php echo $i->menu_id; ?>"><?php echo ($i->menu_id=="0") ? '' : $pmenu->name;?></a></td>
	       <td>
	       <?php
	       		$category = new WPRESTO_Category();
				$cat = $category->get_all($i->category_id);
				echo $cat[0]->name;
				unset($category);
	       ?>
	       </td>
	       <td><?php echo $i->price;?></td>
	       <td>
	         <?php if(!empty($i->image)):?>
	           <img class="WPRESTO_preview_index_image" src="<?php echo $i->image;?>" width="40"/>
	         <?php endif;?>
	       </td>
	       <td><?php echo $i->display_order; ?></td>
	       <td><?php echo ($i->active == 1) ? 'Yes' : 'No'; ?></td>
	       <td><a href="admin.php?page=main_menu&action=edit_single_item&referrer=fooditems&item_id=<?php echo $i->id; ?>">Edit Item</a></td>
	       <td>
	         <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	           <input type="hidden" name="wpresto_delete[menu_id]" value="<?php echo $i->menu_id;?>" />
	           <input type="hidden" name="wpresto_delete[item_name]" value="<?php echo $i->name;?>" />
	           <input type="hidden" name="wpresto_delete[item_id]" value="<?php echo $i->id;?>" />
	           <input type="submit" class="button" name="wpresto_delete[item]" value="Delete" onclick="return confirm('Are you sure you want to delete <?php echo $i->name;?>?')"/>
	         </form>
	       </td>
	     </tr>
    <?php endforeach; ?>
  </tbody>
  </table>

  <div class="WPRESTO-admin-nav">
    <p>
      <a class="button-primary" href="admin.php?page=main_menu&action=new_item">+ Add New Food Item</a>&nbsp;
      <a class="button" href="admin.php?page=main_menu">&laquo;back</a>&nbsp;
    </p>
  </div>
</div>
