<div class="wrap wpresto">
  <h2 class="left">Restaurant Menu Manager - Create Category</h2>
  <div class="clear"></div>
  <hr />
<?php

//$menu = new wpresto_Menu((int) $_GET['menu_id']);
//$category = new wpresto_Category();
//$category->menu_id = $menu->id;

if(isset($_POST['save_category'])) {
	$newcat = $_POST['wpresto'];
	$newcat['menu_id'] = $_POST['menu_id'];
	$cat = new WPRESTO_Category();
	if (!isset($_POST['wpresto']['show_title'])) $newcat['show_title']=0;  
	if (!isset($_POST['wpresto']['show_description'])) $newcat['show_description']=0;
	if (!isset($_POST['wpresto']['active'])) $newcat['active']=0;
	$new = $cat->create($newcat);
	echo wpresto_alert_msg("Category <b>" . $newcat['name'] . "</b> successfully added! ");

}

$menuList = new WPRESTO_MENU();
$menus = $menuList->get_all();

?>

  <p class="breadcrumb">
    <a href="admin.php?page=main_menu">Menus</a> &raquo; 
    <a href="admin.php?page=category">Categories</a> &raquo; 
    <a href="">Add Category</a>
  </p>

  <form method="POST" action="#">

    <table class="form-table">
      <tbody>

        <tr valign="top">
          <th scope="row"><label for="wpresto[name_cn]">名称</label></th>
          <td><input name="wpresto[name_cn]" type="text" value="" class="regular-text">
              <span class="description">中文名称.</span>
          </td>
        </tr>
        <tr valign="top">
          <th scope="row"><label for="wpresto[name]">Name</label></th>
          <td><input name="wpresto[name]" type="text" value="" class="regular-text">
              <span class="description">Display name for this category.</span>
          </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[description_cn]">Description</label></th>
            <td><textarea name="wpresto[description_cn]" class="large-text code"></textarea>
                <span class="description">中文描述.</span>
            </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[description]">Description</label></th>
          <td><textarea name="wpresto[description]" class="large-text code"></textarea>
              <span class="description">Display description for this category.</span>
          </td>
        </tr>
		
       <tr valign="top">
          <th scope="row"><label for="wpresto[menu_id]">Link to Menu</label></th>
          <td>
            <select name="menu_id" class="regular-text code">
              <?php foreach($menus as $m):?>
                <option value="<?php echo $m->id;?>"><?php echo $m->name;?></option>
              <?php endforeach;?>
              <option value="0">None (hidden)</option>
            </select>
            <span class="description">Link this category to a menu</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[show_title]">Show Title?</label></th>
          <td>
              <input type="hidden" name="wpresto[show_title]" value="0" />
              <input type="checkbox" name="wpresto[show_title]" value="1" <?php echo ($category->show_title == 1) ? 'checked' : '';?>/>
              <span class="description">Display title text on frontend for category.</span>
          </td>
        </tr>
        <tr valign="top">
          <th scope="row"><label for="wpresto[show_description]">Show Description?</label></th>
          <td>
            <input type="hidden" name="wpresto[show_description]" value="0" />
            <input type="checkbox" name="wpresto[show_description]" value="1" <?php echo ($category->show_description == 1) ? 'checked' : '';?>/>
            <span class="description">Display description text on frontend for category.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[description]">Active</label></th>
          <td>
            <input type="hidden" name="wpresto[active]" value="0" />
            <input type="checkbox" name="wpresto[active]" value="1" checked/>
            <span class="description">Display items in this category.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[display_order]">Display Order</label></th>
          <td>
            <?php $display_order = empty($category->display_order) ? "0" : $category->display_order; ?>
            <input name="wpresto[display_order]" type="text" value="<?php echo $display_order;?>" class="regular-text">
            <span class="description">Order in which this category is displayed to users.</span>
          </td>
        </tr>

      </tbody>
    </table>

    <br />

    <input type="hidden" name="wpresto_crud[category]" value="new" /> 

    <div class="wpresto-admin-nav">
      <p>
        <input class="button-primary" class="left" type="submit" name="save_category" value="Save Category" />&nbsp;
        <a class="button" href="">&laquo;back</a>
      </p>
    </div>


  </form>

</div>
