<div class="wrap wpresto">
  <h2 class="left">Restaurant Menu Manager - Create New Menu</h2>
  <div class="clear"></div>
  <hr />

<?php

if(isset($_POST['save_menu'])) {
	
	$newMenu = $_POST['wpresto'];
	$menu = new WPRESTO_MENU();
	$new = $menu->create($newMenu);
	echo wpresto_alert_msg("Menu <b>" . $newMenu['name'] . "</b> successfully added! ");
}

$category = new wpresto_Category();
$categories = $category->get_all();

?>


  <p class="wpresto-breadcrumb">
    <a href="admin.php?page=main_menu">Menus</a> &raquo; 
    <a href="">Create New Menu</a>
  </p>

  <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <table class="form-table">
      <tbody>
        <tr valign="top">
          <th scope="row"><label for="wpresto[name_cn]">名称</label></th>
          <td><input name="wpresto[name_cn]" type="text" value="" class="regular-text">
              <span class="description">中文名称.</span>
          </td>
        </tr>
        <tr valign="top">
          <th scope="row"><label for="wpresto[name]">Name</label></th>
          <td><input name="wpresto[name]" type="text" value="" class="regular-text">
              <span class="description">Display name for this menu.</span>
          </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[show_title]">Show title on frontend?</label></th>
          <td>
            <input type="checkbox" name="wpresto[show_title]" value="1"/>
            <span class="description">Display menu title?</span>
          </td>
        </tr>

        <tr valign="top">
            <th scope="row"><label for="wpresto[description_cn]">描述</label></th>
            <td><textarea name="wpresto[description_cn]" class="large-text code"></textarea>
                <span class="description">中文描述.</span>
            </td>
        </tr>

        <tr valign="top">
          <th scope="row"><label for="wpresto[description]">Description</label></th>
          <td><textarea name="wpresto[description]" class="large-text code"></textarea>
              <span class="description">Display description for this menu.</span>
          </td>
        </tr>
	
        <!--tr valign="top">
          <th scope="row"><label for="print_view_url">Print View URL</label></th>
          <td>
              <a href="" target="_blank">URL HERE</a>
              <br />
              <span class="description">URL to the printer friendly view of this menu.</span>
          </td>
        </tr-->

        <tr valign="top">
          <th scope="row"><label for="wpresto[show_link]">Show print link?</label></th>
          <td>
            <input type="checkbox" name="wpresto[show_link]" value="1"/>
            <span class="description">Shows the link to the printer friendly version of the menu on the frontend.</span>
          </td>
        </tr>   
        
      </tbody>
    </table>
   
    <div class="wpresto-admin-nav">
      <p>
        <input class="button-primary" class="left" type="submit" name="save_menu" value="Save Menu" />&nbsp;
        <a class="button" href="admin.php?page=main_menu">&laquo;Back To Menu List</a>&nbsp;
      </p>
    </div>

  </form>

</div>
