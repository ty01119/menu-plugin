<?php
class WPRESTO_Category{
 
/*  public $id, $name, $description;*/
  public function __construct($id = 'ALL') { 
    $this->id = '';
    $this->name = 'New';
    $this->name_cn = 'New';
    $this->description = '';
    $this->description_cn = '';
	  $this->active = 1;
    $this->show_description = 1;
    $this->show_title = 1;
    $this->layout = 'one-column';
    if(is_numeric($id)) $this->load_category($id);
  }


 
  /* @return all categories in the DB */
  public static function get_all( $arg = 'ALL' ){
    global $wpdb;
   
    $where = array();
    if( !empty( $category_ids ) && is_array( $category_ids ) ) {
      $where[] = 'id IN ('.implode( ',', $category_ids ).')';
    }
    if( is_numeric($menu_id) ){
      $where[] = 'menu_id="'.$menu_id.'"';
    }
    $where = empty($where) ? '': ' WHERE ' . implode( ' AND ', $where );
	
	  $where = '';
	  if($arg!="ALL") $where = "WHERE id=" . $arg;
	  $sql = 'SELECT * FROM '.WPPRESTO_CATEGORY_DB.' '.$where.' ORDER BY display_order DESC';
    $categories = $wpdb->get_results($sql);
    //return $categories;
    return stripslashes_deep($categories);
    //return $sql;
  }
  
  
  public static function get_all_by_menu( $menu_id ){
    global $wpdb;
	  $sql = 'SELECT * FROM '.WPPRESTO_CATEGORY_DB. ' WHERE menu_id='.$menu_id.' ORDER BY display_order DESC';
    $categories = $wpdb->get_results($sql);
    return stripslashes_deep($categories);
  }

  public static function get_all_actived_by_menu( $menu_id ){
    global $wpdb;
    $sql = 'SELECT * FROM '.WPPRESTO_CATEGORY_DB. ' WHERE menu_id='.$menu_id.' AND active=1 ORDER BY display_order DESC';
    $categories = $wpdb->get_results($sql);
    return stripslashes_deep($categories);
  }
  
  /* Create category object from ID */
  private function load_category($id){
    global $wpdb;
    $category = $wpdb->get_results('SELECT * FROM '.WPPRESTO_CATEGORY_DB.' WHERE id="'.$id.'" LIMIT 1', ARRAY_A);
    $category = $category[0];
    if(empty($category)) return '';
    $this->id = $category['id'];
    $this->name = stripslashes($category['name']);
    $this->name_cn = stripslashes($category['name_cn']);
    $this->description = stripslashes($category['description']);
    $this->description_cn = stripslashes($category['description_cn']);
    $this->layout = stripslashes($category['layout']);
    $this->active = stripslashes($category['active']);
    $this->show_title = stripslashes($category['show_title']);
    $this->show_description = stripslashes($category['show_description']);
    $this->display_order = stripslashes($category['display_order']);
    $this->menu_id = stripslashes($category['menu_id']);
  }

  /* Saves category on admin update */
  public static function update($category){
    global $wpdb;
    $wpdb->update(WPPRESTO_CATEGORY_DB, $category, array('id'=>$category['id']));
  }

  /* Creates category in DB */
  public static function create($category){
    global $wpdb;
    $wpdb->insert(WPPRESTO_CATEGORY_DB, $category);
    return $wpdb->insert_id;
  }
  /* Deletes menu from DB */
  public static function destroy($category_id){
    global $wpdb;
    if(empty($category_id)) return '';
    $wpdb->query('DELETE FROM '.WPPRESTO_CATEGORY_DB.' WHERE id="'.$category_id.'"'); 
  }

}
?>
