<?php
class WPRESTO_MENU {
 
  /*public $id, $name, $description;*/

  public function __construct($id = 'ALL') { 
    $this->id = '';
    $this->name = '';
    $this->name_cn = '';
    $this->show_title = 1;
    $this->description = '';
    $this->description_cn = '';
    $this->show_link = 1;
    $this->rss2 = 1;
    $this->shop_name = '';
    $this->shop_name_cn = '';
    $this->address = '';
    $this->phone = '';
    $this->open_time = '';
    $this->multi_lang = '';

    if(is_numeric($id)) $this->load_menu($id);
  }

  /* @return all menus in the DB */ 
  /* Create menu object from ID */
  private function load_menu($id){
    global $wpdb;
    $menu = $wpdb->get_results('SELECT * FROM '.WPPRESTO_MENU_DB.' WHERE id="'.$id.'" LIMIT 1', ARRAY_A);
    $menu = $menu[0];
    if(empty($menu)) return '';

    $this->id = $menu['id'];
    $this->name = stripslashes($menu['name']);
    $this->name_cn = stripslashes($menu['name_cn']);
    $this->show_title = (int) $menu['show_title'];
    $this->description = stripslashes($menu['description']);
    $this->description_cn = stripslashes($menu['description_cn']);
    $this->shop_name = stripslashes($menu['shop_name']);
    $this->shop_name_cn = stripslashes($menu['shop_name_cn']);
    $this->address = stripslashes($menu['address']);
    $this->phone = stripslashes($menu['phone']);
    $this->open_time = stripslashes($menu['open_time']);
    $this->show_link = (int) $menu['show_link'];
  }

    public function get_all(){
		global $wpdb;
		$menus = $wpdb->get_results('SELECT * FROM '.WPPRESTO_MENU_DB);
		//return stripslashes_deep($menus);
		return $menus;
  }
  
  /* Set menu object from name */
  public function get_by_name($name){
    global $wpdb;
    $menu = $wpdb->get_results('SELECT * FROM '.WPPRESTO_MENU_DB.' WHERE name="'.$name.'" LIMIT 1', ARRAY_A);
    $menu = $menu[0];
    if(empty($menu)) return '';
    return $menu;
  }

  /* Set menu object from name */
  public static function get_by_id($id){
    global $wpdb;
    $sql = 'SELECT * FROM '.WPPRESTO_MENU_DB.' WHERE id="'.$id.'" LIMIT 1';
    $menu = $wpdb->get_results($sql, ARRAY_A);
    $menu = $menu[0];
    $menu['print_url'] = site_url()."?wpresto-action=print&menu_id=".$menu['id'];
    return stripslashes_deep($menu);
  }

  /* Saves menu on admin update */
  public function update($menu){
    global $wpdb;
    $wpdb->update(WPPRESTO_MENU_DB, $menu, array('id'=>$menu['id'])); 

  }

  /* Saves menu on admin update */
  public function create($menu){
    global $wpdb;
    $wpdb->insert(WPPRESTO_MENU_DB, $menu); 
    return $wpdb->insert_id;
  }

  /* Saves menu on admin update */
  public function destroy($menu_id){
    global $wpdb;
    if(empty($menu_id)) return '';
    $wpdb->query('DELETE FROM '.WPPRESTO_MENU_DB.' WHERE id="'.$menu_id.'"'); 
  }
}
?>
