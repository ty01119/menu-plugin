<?php 

/*** Displays Printer Friendly HTML Menu on frontend */
function wpresto_print_menu($id){

  global $wpdb;
  if(empty($id) || !is_numeric($id) ) return '';
  /* Get Menu, Categories, and Items */
  $menu = WPRESTO_MENU::get_by_id($id);
  if(empty($menu['id'])) return "Menu not found.";
 
  $s = '<html>
          <head>
            <meta charset="utf-8">
            <title>'.$menu['name'].'</title>
            <link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/style1.css" type="text/css" media="all" />           
            <link rel="stylesheet" href="'.WPPRESTO_PLUGIN_URL.'css/view-style.css?ver=3.9" type="text/css" media="all" />
            <link rel="stylesheet" href="'.WPPRESTO_PLUGIN_URL.'css/b.min.css" type="text/css" media="all" />
          </head>
          <body style="padding:10px">'.wpresto_display_menu($id).'
          <center><button class="button" onClick="window.print()"> Print </button></center>
          </body>
        </html>';
  return $s;
}

?>